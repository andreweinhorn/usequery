import React, {useState} from 'react';
import {Text, View} from 'react-native';
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  gql,
  useQuery,
} from '@apollo/client';

const client = new ApolloClient({
  uri: 'https://api.spacex.land/graphql/',
  cache: new InMemoryCache(),
});

const LAUNCHES = gql`
  query Launches {
    launches(limit: 5) {
      mission_name
      mission_id
    }
  }
`;

function Launches1() {
  const [data, setData] = useState('Loading ...');
  client
    .query({
      query: LAUNCHES,
    })
    .then(result => {
      console.log('Areas 1 Data: ', result.data);
      setData('Data ...');
    });
  return <Text>{data}</Text>;
}

function Launches2() {
  const {loading, error, data} = useQuery(LAUNCHES);

  if (loading) {
    console.log('Areas2 in loading state ...');
    return <Text>Loading ...</Text>;
  }
  if (data) {
    console.log('Areas 2 Data: ', data);
    return <Text>Data ...</Text>;
  }
}

const App = () => {
  return (
    <ApolloProvider client={client}>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>Hello World {'\n'}</Text>
        <Text>
          client.query: <Launches1 />
        </Text>
        <Text>
          client.query: <Launches2 />
        </Text>
      </View>
    </ApolloProvider>
  );
};

export default App;
